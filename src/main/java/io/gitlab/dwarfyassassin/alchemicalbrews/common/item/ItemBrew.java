package io.gitlab.dwarfyassassin.alchemicalbrews.common.item;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.brew.BrewEffect;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.brew.BrewIngredient;
import lotr.common.LOTRAchievement;
import lotr.common.LOTRLevelData;
import lotr.common.entity.npc.LOTREntityNPC;
import lotr.common.item.LOTRItemMug;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class ItemBrew extends LOTRItemMug {
    @SideOnly(value = Side.CLIENT)
    private IIcon[] drinkIcons;
    @SideOnly(value = Side.CLIENT)
    private IIcon[] liquidIcons;

    public ItemBrew() {
        super(0.0f);
    }
    
    @SideOnly(value = Side.CLIENT)
    @Override
    public int getColorFromItemStack(ItemStack itemstack, int pass) {
        if(pass == 0) {
            return super.getColorFromItemStack(itemstack, pass);
        }
        
        return ItemBrew.getBrewTag(itemstack).getInteger("BrewColor");
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public boolean requiresMultipleRenderPasses() {
        return true;
    }
    
    @Override
    public IIcon getIconFromDamageForRenderPass(int damage, int pass) {
        if(pass == 0) {
            return drinkIcons[Vessel.forMeta(damage/LOTRItemMug.vesselMeta).id];
        }
        else {
            return liquidIcons[Vessel.forMeta(damage/LOTRItemMug.vesselMeta).id];
        }
    }
    
    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIconIndex(ItemStack itemstack) {
        return drinkIcons[Vessel.forMeta(itemstack.getItemDamage()/LOTRItemMug.vesselMeta).id];
    }
    
    @Override
    public void registerIcons(IIconRegister iconregister) {
        this.drinkIcons = new IIcon[Vessel.values().length];
        this.liquidIcons = new IIcon[Vessel.values().length];
        for(int i = 0; i < Vessel.values().length; ++i) {
            this.drinkIcons[i] = iconregister.registerIcon(this.getIconString() + "_" + Vessel.values()[i].name);
            this.liquidIcons[i] = iconregister.registerIcon(this.getIconString() + "_" + Vessel.values()[i].name + "_liquid");
        }
    }
    
    
    private List<PotionEffect> convertPotionEffectsForStrength(ItemStack itemstack, float strength) {
        ArrayList<PotionEffect> list = new ArrayList<PotionEffect>();
        for(PotionEffect base : BrewEffect.getEffects(itemstack)) {
            PotionEffect modified = new PotionEffect(base.getPotionID(), (int) (base.getDuration() * strength), base.getAmplifier());
            list.add(modified);
        }
        return list;
    }
    
    @SideOnly(value = Side.CLIENT)
    @Override
    public void addInformation(ItemStack itemstack, EntityPlayer entityplayer, List list, boolean flag) {
        if(this.isBrewable) {
            float strength = LOTRItemMug.getStrength(itemstack);
            list.add(LOTRItemMug.getStrengthSubtitle(itemstack));
            if(this.alcoholicity > 0.0f) {
                EnumChatFormatting c = EnumChatFormatting.GREEN;
                float f = this.alcoholicity * strength * 10.0f;
                c = f < 2.0f ? EnumChatFormatting.GREEN : (f < 5.0f ? EnumChatFormatting.YELLOW : (f < 10.0f ? EnumChatFormatting.GOLD : (f < 20.0f ? EnumChatFormatting.RED : EnumChatFormatting.DARK_RED)));
                list.add((c) + StatCollector.translateToLocal("item.lotr.drink.alcoholicity") + ": " + String.format("%.2f", Float.valueOf(f)) + "%");
            }
            LOTRItemMug.addPotionEffectsToTooltip(itemstack, entityplayer, list, flag, convertPotionEffectsForStrength(itemstack, strength));
        }
    }
    
    @Override
    public ItemStack onEaten(ItemStack itemstack, World world, EntityPlayer entityplayer) {
        Vessel vessel = LOTRItemMug.getVessel(itemstack);
        float strength = LOTRItemMug.getStrength(itemstack);
        float foodStrength = LOTRItemMug.getFoodStrength(itemstack);
        if(entityplayer.canEat(false)) {
            entityplayer.getFoodStats().addStats(Math.round(this.foodHealAmount * foodStrength), this.foodSaturationAmount * foodStrength);
        }
        if(this.alcoholicity > 0.0f) {
            int duration;
            float alcoholPower = this.alcoholicity * strength;
            int tolerance = LOTRLevelData.getData(entityplayer).getAlcoholTolerance();
            if(tolerance > 0) {
                float f = (float) Math.pow(0.99, tolerance);
                alcoholPower *= f;
            }
            if(!world.isRemote && itemRand.nextFloat() < alcoholPower && (duration = (int) (60.0f * (1.0f + itemRand.nextFloat() * 0.5f) * alcoholPower)) >= 1) {
                int durationTicks = duration * 20;
                entityplayer.addPotionEffect(new PotionEffect(Potion.confusion.id, durationTicks));
                LOTRLevelData.getData(entityplayer).addAchievement(LOTRAchievement.getDrunk);
                int toleranceAdd = Math.round(duration / 20.0f);
                LOTRLevelData.getData(entityplayer).setAlcoholTolerance(tolerance += toleranceAdd);
            }
        }
        if(!world.isRemote && this.shouldApplyPotionEffects(itemstack, entityplayer)) {
            List<PotionEffect> effects = this.convertPotionEffectsForStrength(itemstack, strength);
            for(PotionEffect effect : effects) {
                entityplayer.addPotionEffect(effect);
            }
        }
        if(this.damageAmount > 0) {
            entityplayer.attackEntityFrom(DamageSource.magic, this.damageAmount * strength);
        }
        if(!world.isRemote && this.curesEffects) {
            entityplayer.curePotionEffects(new ItemStack(Items.milk_bucket));
        }
        if(!world.isRemote) {
            //TODO achievement?
        }
        return !entityplayer.capabilities.isCreativeMode ? new ItemStack(vessel.getEmptyVesselItem()) : itemstack;
    }
    
    public void applyToNPC(LOTREntityNPC npc, ItemStack itemstack) {
        float strength = LOTRItemMug.getStrength(itemstack);
        npc.heal(this.foodHealAmount * strength);
        List<PotionEffect> effects = this.convertPotionEffectsForStrength(itemstack, strength);
        for(PotionEffect effect : effects) {
            npc.addPotionEffect(effect);
        }
        if(this.damageAmount > 0) {
            npc.attackEntityFrom(DamageSource.magic, this.damageAmount * strength);
        }
        if(this.curesEffects) {
            npc.curePotionEffects(new ItemStack(Items.milk_bucket));
        }
    }
    
    
    public static NBTTagCompound getBrewTag(ItemStack itemstack) {
        NBTTagCompound nbt = itemstack.getTagCompound();
        if(nbt == null) {
            nbt = new NBTTagCompound();
            itemstack.setTagCompound(nbt);
        }
        
        return nbt.getCompoundTag("BrewInfo");
    }
    
    public static void setBrewTag(ItemStack itemstack, NBTTagCompound tag) {
        NBTTagCompound nbt = itemstack.getTagCompound();
        if(nbt == null) {
            nbt = new NBTTagCompound();
            itemstack.setTagCompound(nbt);
        }
        
        nbt.setTag("BrewInfo", tag);
    }
    
    public static void setBrewIngredients(ItemStack itemstack, List<BrewIngredient> ingredients) {
        NBTTagCompound brewTag = ItemBrew.getBrewTag(itemstack);
        
        NBTTagList list = new NBTTagList();
        int r = 0;
        int g = 0;
        int b = 0;
        
        for(BrewIngredient ingredient : ingredients) {
            list.appendTag(new NBTTagString(ingredient.getName()));
            Color c = ingredient.getColor();
            r += c.getRed();
            g += c.getGreen();
            b += c.getBlue();
        }
        
        r /= ingredients.size();
        g /= ingredients.size();
        b /= ingredients.size();
        int c = (r << 16) | (g << 8) | (b);
        
        
        brewTag.setTag("BrewIngredients", list);
        brewTag.setInteger("BrewColor", c);
        
        
        ItemBrew.setBrewTag(itemstack, brewTag);
    }

}
