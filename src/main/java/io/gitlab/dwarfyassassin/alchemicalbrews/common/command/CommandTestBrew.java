package io.gitlab.dwarfyassassin.alchemicalbrews.common.command;

import java.util.ArrayList;
import java.util.List;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.AlchemicalBrews;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.brew.BrewEffect;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.brew.BrewIngredient;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.item.ItemBrew;
import lotr.common.item.LOTRItemMug;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.ChatComponentText;

public class CommandTestBrew extends CommandBase {

    @Override
    public String getCommandName() {
        return "testbrew";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return null;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        EntityPlayerMP player = getCommandSenderAsPlayer(sender);
        ItemStack stack1 = player.inventory.getStackInSlot(0);
        ItemStack stack2 = player.inventory.getStackInSlot(1);
        ItemStack stack3 = player.inventory.getStackInSlot(2);
        
        if(stack1 == null || !BrewIngredient.isBrewIngredient(stack1.getItem())) {
            player.addChatMessage(new ChatComponentText("Need to hold ingredient"));
            return;
        }
        
        List<BrewIngredient> list = new ArrayList<BrewIngredient>();
        list.add(BrewIngredient.getBrewIngredient(stack1.getItem()));
        if(stack2 != null && BrewIngredient.isBrewIngredient(stack2.getItem())) list.add(BrewIngredient.getBrewIngredient(stack2.getItem()));
        if(stack3 != null && BrewIngredient.isBrewIngredient(stack3.getItem())) list.add(BrewIngredient.getBrewIngredient(stack3.getItem()));
        
        
        int damage = 0;
        if(args.length >= 1) damage = CommandBase.parseIntWithMin(player, args[0], 0);
        
        ItemStack brew = new ItemStack(AlchemicalBrews.itemBrew, 1, damage);
        ItemBrew.setBrewIngredients(brew, list);
        player.inventory.addItemStackToInventory(brew);
        
        
        List<PotionEffect> effects = new ArrayList<PotionEffect>();
        for(PotionEffect base : BrewEffect.getEffects(brew)) {
            float strength = LOTRItemMug.getStrength(brew);
            PotionEffect modified = new PotionEffect(base.getPotionID(), (int) (base.getDuration() * strength), base.getAmplifier());
            effects.add(modified);
        }
        
        for(PotionEffect effect : effects) {
            player.addChatMessage(new ChatComponentText("Effects: " + effect.toString()));
        }
        
        if(effects.size() == 0) {
            player.addChatMessage(new ChatComponentText("No effect"));
        }
        player.addChatMessage(new ChatComponentText("---"));
    }

}
