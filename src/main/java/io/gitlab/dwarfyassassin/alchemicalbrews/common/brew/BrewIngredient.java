package io.gitlab.dwarfyassassin.alchemicalbrews.common.brew;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.AlchemicalBrews;
import lotr.common.LOTRMod;
import lotr.common.item.LOTRPoisonedDrinks;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.potion.Potion;

public class BrewIngredient {
    private static Map<Potion, Integer> maxPotions = new HashMap<Potion, Integer>();
    private static Map<Potion, Integer> usedPotions = new HashMap<Potion, Integer>();
    private static List<BrewIngredient> ingredients = new ArrayList<BrewIngredient>();
    
    public static BrewIngredient athelas;
    public static BrewIngredient dwarfWort;
    public static BrewIngredient niphredil;
    public static BrewIngredient elanor;
    public static BrewIngredient morgulFlower;
    public static BrewIngredient morgulShroom;
    public static BrewIngredient rhinoHorn;
    // static varaible
    // static BrewIngredient
    
    
    private String name;
    private Item item;
    private Color ingredientColor;
    private Potion[] effects = new Potion[3]; 
    
    public static void initIngredients(long seed) {
        Random random = new Random(seed);
        athelas = new BrewIngredient(LOTRMod.athelas, Potion.regeneration, Potion.moveSpeed, 0x13d16f);
        dwarfWort = new BrewIngredient(LOTRMod.dwarfHerb, Potion.nightVision, Potion.digSlowdown, 0x1ad9d6);
        niphredil = new BrewIngredient(LOTRMod.niphredil, Potion.moveSpeed, Potion.invisibility, 0xf4f74a);
        elanor = new BrewIngredient(LOTRMod.elanor, Potion.damageBoost, Potion.fireResistance, 0xdbb407);
        morgulFlower = new BrewIngredient(LOTRMod.morgulFlower, Potion.nightVision, Potion.invisibility, 0xc3f7e3);
        morgulShroom = new BrewIngredient(LOTRMod.morgulShroom, Potion.wither, Potion.damageBoost, 0x8a1e0e);
        rhinoHorn = new BrewIngredient(LOTRMod.rhinoHorn, Potion.confusion, Potion.resistance, 0x382724);
        
        
        maxPotions.put(Potion.regeneration, 2);
        maxPotions.put(Potion.damageBoost, 3);
        maxPotions.put(Potion.resistance, 2);
        maxPotions.put(Potion.wither, 3);
        maxPotions.put(LOTRPoisonedDrinks.killingPoison, 2);
        
        String potions = "";
        for(BrewIngredient ingredient : ingredients) {
            Potion potion = null;
            
            boolean foundPotion = false;
            while(!foundPotion) {
                potion = Potion.potionTypes[random.nextInt(Potion.potionTypes.length)];
                foundPotion = true;
                
                if(potion == null) {
                    foundPotion = false;
                }
                else if(ingredient.effects[0].isBadEffect() && ingredient.effects[1].isBadEffect() && potion.isBadEffect()) {
                    foundPotion = false;
                }
                else if(!ingredient.effects[0].isBadEffect() && !ingredient.effects[1].isBadEffect() && !potion.isBadEffect()) {
                    foundPotion = false;
                }
                else if(usedPotions.getOrDefault(potion, 0) >= maxPotions.getOrDefault(potion, Integer.MAX_VALUE)) {
                    foundPotion = false;
                }
            }
            
            ingredient.effects[2] = potion;
            usedPotions.put(potion, usedPotions.getOrDefault(potion, 0) + 1);
            potions += ingredient.name + ": " + potion.id + " - ";
        }
        
        AlchemicalBrews.logger.info("Registered random potions effects. " + potions);
    }

    public static boolean isBrewIngredient(Item item) {
        return getBrewIngredient(item) != null;
    }
    
    public static BrewIngredient getBrewIngredient(Item item) {
        for(BrewIngredient ingredient : ingredients) {
            if(ingredient.item == item) return ingredient;
        }
        
        return null;
    }
    
    public static BrewIngredient getBrewIngredient(String name) {
        for(BrewIngredient ingredient : ingredients) {
            if(ingredient.name.equals(name)) return ingredient;
        }
        
        return null;
    }
    
    public BrewIngredient(Block ingredient, Potion effect1, Potion effect2, int color) {
        this(Item.getItemFromBlock(ingredient), effect1, effect2, color);
    }
    
    public BrewIngredient(Item item, Potion effect1, Potion effect2, int color) {
        this.item = item;
        name = item.getUnlocalizedName();
        effects[0] = effect1;
        effects[1] = effect2;
        ingredientColor = new Color(color);
        
        ingredients.add(this);
        usedPotions.put(effect1, usedPotions.getOrDefault(effect1, 0) + 1);
        usedPotions.put(effect2, usedPotions.getOrDefault(effect2, 0) + 1);
    }
    
    public Potion[] getEffects() {
        return effects;
    }
    
    public String getName() {
        return name;
    }
    
    public Color getColor() {
        return ingredientColor;
    }
    
    

}
