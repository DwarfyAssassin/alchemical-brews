package io.gitlab.dwarfyassassin.alchemicalbrews.common.brew;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.item.ItemBrew;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.util.Constants.NBT;

public class BrewEffect {
    
    public static List<PotionEffect> calcEffects(BrewIngredient... ingredients) {
        List<PotionEffect> effects = new ArrayList<PotionEffect>();
        
        Map<Potion, Integer> potions = new HashMap<Potion, Integer>();
        Map<BrewIngredient, Integer> ingredientsCount = new HashMap<BrewIngredient, Integer>();
        for(BrewIngredient ingredient : ingredients) {
            ingredientsCount.put(ingredient, ingredientsCount.getOrDefault(ingredient, 0) + 1);
            
            for(Potion potion : ingredient.getEffects()) {
                potions.put(potion, potions.getOrDefault(potion, 0) + 1);
            }
        }
        
        for(Potion potion : potions.keySet()) {
            int i = potions.get(potion);
            
            if(i >= 2) {
                int amplifier = i - 2; // starts at 0
                int duration = (int) Math.round(20*45 * potion.getEffectiveness() / Collections.max(ingredientsCount.values()));
                
                effects.add(new PotionEffect(potion.id, duration, amplifier));
            }
            
        }
        
        return effects;
    }
    
    public static List<PotionEffect> getEffects(ItemStack itemstack) {
        List<BrewIngredient> ingredients = new ArrayList<BrewIngredient>();
        NBTTagList nbttaglist = ItemBrew.getBrewTag(itemstack).getTagList("BrewIngredients", NBT.TAG_STRING);
        for (int i = 0; i < nbttaglist.tagCount(); ++i) {
            String ingredientName = nbttaglist.getStringTagAt(i);
            BrewIngredient ingredient = BrewIngredient.getBrewIngredient(ingredientName);
            if(ingredient != null) {
                ingredients.add(ingredient);
            }
        }
        
        return calcEffects(ingredients.toArray(new BrewIngredient[3]));
    }

}
