package io.gitlab.dwarfyassassin.alchemicalbrews.common;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.brew.BrewIngredient;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.command.CommandTestBrew;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.item.ItemBrew;
import net.minecraft.item.Item;

@Mod(modid = AlchemicalBrews.MODID, name = AlchemicalBrews.NAME, version = AlchemicalBrews.VERSION, dependencies="required-after:lotr;")
public class AlchemicalBrews {
    public static final String MODID = "alchemicalbrews";
    public static final String NAME = "Alchemical Brews";
    public static final String VERSION = "0.0.1";

    public static Logger logger = LogManager.getLogger("AlchemicalBrews");

    @SidedProxy(clientSide="io.gitlab.dwarfyassassin.alchemicalbrews.client.ClientProxy", serverSide="io.gitlab.dwarfyassassin.alchemicalbrews.common.CommonProxy")
    public static CommonProxy proxy;
    
    private static long seed;
    
    public static Item itemBrew;


    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        itemBrew = new ItemBrew();
        
        itemBrew.setUnlocalizedName("itemBrew");
        itemBrew.setTextureName(MODID + ":" + "item_brew");
        GameRegistry.registerItem(itemBrew, "item_brew");
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init(event);
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit(event);
    }
    
    @EventHandler
    public void postInit(FMLServerStartingEvent event) {
        event.registerServerCommand(new CommandTestBrew());
        
        seed = event.getServer().getEntityWorld().getWorldInfo().getSeed();
        logger.info("AlchemicalBrews seed is: " + seed);
        BrewIngredient.initIngredients(seed);
    }
    
    
    
    public long getSeed() {
        return seed;
    }
}
