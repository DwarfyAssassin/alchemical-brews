package io.gitlab.dwarfyassassin.alchemicalbrews.client;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.CommonProxy;

public class ClientProxy extends CommonProxy {

    public static ClientEventHandler eventHandler;
    
    @Override
    public void init(FMLInitializationEvent event) {
        super.init(event);
        eventHandler = new ClientEventHandler();
    }
}
