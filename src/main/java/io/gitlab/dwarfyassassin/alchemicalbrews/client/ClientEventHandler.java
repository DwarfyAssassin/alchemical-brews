package io.gitlab.dwarfyassassin.alchemicalbrews.client;

import java.util.List;
import org.lwjgl.input.Keyboard;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import io.gitlab.dwarfyassassin.alchemicalbrews.common.brew.BrewIngredient;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;

public class ClientEventHandler {
    private Minecraft mc;
    
    public ClientEventHandler() {
        MinecraftForge.EVENT_BUS.register(this);
        mc = Minecraft.getMinecraft();
    }
    
    @SubscribeEvent
    public void getItemTooltip(ItemTooltipEvent event) {
        ItemStack itemstack = event.itemStack;
        EntityPlayer entityplayer = event.entityPlayer;
        List<String> tooltip = event.toolTip;
        
        // TODO while in special GUI
        BrewIngredient ingredient = BrewIngredient.getBrewIngredient(itemstack.getItem());
        if(ingredient != null && Keyboard.isKeyDown(mc.gameSettings.keyBindSneak.getKeyCode())) {
            Potion[] effects = ingredient.getEffects();
            tooltip.add(StatCollector.translateToLocalFormatted("ingredient.item.tooltip", StatCollector.translateToLocalFormatted(effects[0].getName()), StatCollector.translateToLocalFormatted(effects[1].getName()), StatCollector.translateToLocalFormatted(effects[2].getName())));
        }
        
        //EnumChatFormatting.BLUE
        
    }

}
